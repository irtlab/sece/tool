#!/usr/bin/env bash
set -euo pipefail
dir=$(dirname $(realpath -s $(readlink -f $BASH_SOURCE)))

exec node "$dir/dist/sece.js" "$@"
