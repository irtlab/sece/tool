import yargs from 'yargs';
import { loadConfig, saveConfig, ensureWritableConfig, type Config } from './config.ts';
import commands from './cmds/index.ts';


export function ensureRoot() {
    if (process.getuid!() !== 0)
        throw new Error('This command must be run under root');
}


export interface CmdlineArgs {
    api: string;
    config: string;
    id: string;
}


// Support for names with components delimited by the dot character. Recursively
// create any objects and return the leaf object and the name of the attribute
// that should be manipulated on that object.
function descent(obj: any, name: string) {
    const c = name.split('.');
    for(let i = 0; i < c.length - 1; i++) {
        const old = obj;
        obj = obj[c[i]] || {};
        old[c[i]] = obj;
    }
    return { obj, key: c[c.length - 1] };
}


async function set({ config, name, value }: { config: Partial<Config>, name: string, value: string }) {
    await ensureWritableConfig();

    let v;
    switch(name) {
        case 'accounts':
            v = value.split(',').map(v => v.trim()).filter(v => v.length !== 0);
            break;

        default:
            v = `${value}`;
            break;
    }

    const { obj, key } = descent(config, name);
    obj[key] = v;
}


function get({ config, name }: { config: Partial<Config>, name: string }) {
    if (name in config) {
        const { obj, key } = descent(config, name);
        console.log(`${obj[key]}`);
    } else {
        process.exit(1);
    }
}


async function unset({ config, name }: { config: Partial<Config>, name: string }) {
    await ensureWritableConfig();
    delete config[name];
    const { obj, key } = descent(config, name);
    delete obj[key];
}


await yargs(process.argv.slice(2))
    .strict()
    .scriptName('sece')
    .option('api', {
        alias    : 'a',
        describe : 'SECE service API URL',
        default  : process.env.SECE_API || 'https://api.sece.io',
        type     : 'string'
    })
    .option('config', {
        alias    : 'c',
        describe : 'Configuration file',
        default  : process.env.CONFIG || '/data/sece/config.json',
        type     : 'string'
    })
    .option('id', {
        alias    : 'i',
        describe : 'Identity file',
        default  : process.env.IDENTITY || '/data/wireguard/id',
        type     : 'string'
    })
    .middleware(loadConfig)
    .command('set <name> <value>', 'Set setting <name> to <value>', (() => { /* empty */ }) as any, set as any)
    .command('unset <name>', 'Delete setting <name>', (() => { /* empty */ }) as any, unset as any)
    .command('get <name>', 'Get the value for setting <name>', (() => { /* empty */ }) as any, get as any)
    .command(commands as any)
    .demandCommand()
    .version(false)
    .parse();

await saveConfig();
