import nacl from 'tweetnacl';
import { randomInt } from '@janakj/lib/random';
import fetch, { type RequestInit } from 'node-fetch';
import * as authHeader from 'auth-header';


function generateNonce() {
    const n = Buffer.alloc(nacl.box.nonceLength);
    for(let i = 0; i < n.length; i++)
        n[i] = randomInt(0, 256);
    return n;
}


function generateAuthResponse(keyPair: nacl.BoxKeyPair, hdr: authHeader.Token) {
    if (hdr.scheme.toLowerCase() !== 'sece-wireguard')
        throw new Error('Unsupported authentication scheme');

    const { challenge, 'server-key': sk } = hdr.params;
    if (typeof challenge !== 'string' || typeof sk !== 'string')
        throw new Error('Invalid authentication challenge');

    const nonce = generateNonce();
    const serverKey = Buffer.from(sk, 'base64');

    const msg = nacl.box(Buffer.from(challenge), nonce, serverKey, keyPair.secretKey);
    return `SECE-WireGuard \
challenge="${challenge}", \
response="${Buffer.from(msg).toString('base64')}", \
nonce="${nonce.toString('base64')}", \
server-key="${sk}", \
client-key="${Buffer.from(keyPair.publicKey).toString('base64')}"`;
}


export async function fetchAuth(keyPair: nacl.BoxKeyPair, url: string, opts?: RequestInit | undefined) {
    let res = await fetch(url, opts);
    if (res.status === 401) {
        const hdr = res.headers.get('www-authenticate');
        if (hdr !== null) {
            const challenge = authHeader.parse(hdr);

            res = await fetch(url, {
                ...opts,
                headers: {
                    ...(opts || {}).headers,
                    'Authorization': generateAuthResponse(keyPair, challenge)
                }
            });
        }
    }
    return res;
}
