import path from 'path';
import { promises as fs } from 'fs';
import * as sqrl from 'squirrelly';
import { atomicWrite } from '@janakj/lib/fs';
import { loadIdentity, type Config } from '../config.ts';
import { ensureRoot } from '../sece.ts';
import { type BoxKeyPair } from 'tweetnacl';

export const command = 'configure';
export const describe = 'Re-generate system configuration files from templates';


sqrl.filters.define("neitherNullNorUndefined", v => {
    if (v === undefined || v === null)
        throw new Error('Encountered null or undefined template value');
    return v;
});


class IPv6Addr {
    addr: bigint;
    prefix: number;

    constructor(addr?: bigint, prefix?: number) {
        this.addr   = addr !== undefined ? addr : 0n;
        this.prefix = prefix !== undefined ? prefix : 0;
    }

    static parse(str: string) {
        enum State { start, hextet, delimOrZero, prefix }
        const ht: (number | null)[] = [];
        let state = State.start;
        let prefix = 128;
        let v: number;

        for(let i = 0; i < str.length; i++) {
            switch(state) {
                case State.start:
                    if (str[i] === ':') {
                        state = State.delimOrZero;
                        continue;
                    } else {
                        ht.push(0);
                        state = State.hextet;
                    }
                    break;

                case State.delimOrZero:
                    if (str[i] === ':') {
                        ht.push(null);
                        continue;
                    } else if (str[i] == '/') {
                        state = State.prefix;
                        prefix = 0;
                        continue;
                    } else {
                        ht.push(0);
                        state = State.hextet;
                    }
                    break;

                case State.hextet:
                    if (str[i] === ':') {
                        state = State.delimOrZero;
                        continue;
                    } else if (str[i] == '/') {
                        state = State.prefix;
                        prefix = 0;
                        continue;
                    }
                    break;

                case State.prefix:
                    v = parseInt(str[i]);
                    if (isNaN(v)) throw new Error('Invalid character in IPv6 address prefix length');
                    prefix = prefix * 10 + v;
                    continue;
            }

            v = parseInt(str[i], 16);
            if (isNaN(v)) throw new Error('Invalid character in IPv6 address');
            const n = Number(ht[ht.length - 1]);
            ht[ht.length - 1] = (n << 4) + v;
        }

        for(let i = 0; i < 8; i++) {
            if (ht[i] === null) {
                if (ht.length >= 8) throw new Error('Invalid IPv6 address');
                ht.splice(i, 1, ...Array(8 - ht.length + 1).fill(0));
            }
        }

        if (prefix < 0 || prefix > 128)
            throw new Error('Invalid IPv6 prefix length value');

        let addr = 0n;
        for(let i = 0; i < 8; i++)
            addr = (addr << 16n) + BigInt(ht[i] || 0);

        return new IPv6Addr(addr, prefix);
    }

    subnet(length: number, addr?: bigint) {
        if (length <= 0 || length > 128)
            throw new Error('Invalid subnet length');

        const shift = BigInt(128 - length);
        const netmask = (2n ** BigInt(length) - 1n) << shift;

        const subnet = this.addr & netmask | ((addr || 0n) << shift);
        return new IPv6Addr(subnet, length);
    }

    *subnets(length: number) {
        if (length <= 0 || length > 128)
            throw new Error('Invalid subnet length');

        const shift = BigInt(128 - length);
        const netmask = (2n ** BigInt(length) - 1n) << shift;

        for(let i = BigInt(0); i < 2 ** (length - this.prefix); i = i + 1n) {
            const subnet = this.addr & netmask | (i << shift);
            yield new IPv6Addr(subnet, length);
        }
    }

    netmask() {
        return (2n ** BigInt(this.prefix) - 1n) << BigInt(128 - this.prefix);
    }

    first() {
        return new IPv6Addr(this.addr & this.netmask(), 128);
    }

    static findLongestZeroRun(data: Buffer) {
        const best = { i : -1, len : 0 },
            cur = { i : -1, len : 0 };

        for(let i = 0; i < 8; i++) {
            const v = data.readUInt16BE(i * 2);
            if (v === 0) {
                if (cur.i === -1) {
                    cur.i = i;
                    cur.len = 1;
                } else {
                    cur.len++;
                }
            } else {
                if (cur.i !== -1) {
                    if (best.i === -1 || cur.len > best.len)
                        Object.assign(best, cur);
                    cur.i = -1;
                }
            }
        }

        if (cur.i !== -1) {
            if (best.i === -1 || cur.len > best.len)
                Object.assign(best, cur);
        }

        if (best.i !== -1 && best.len < 2) best.i = -1;
        return best;
    }

    toWire() {
        const data = Buffer.alloc(16);
        for(let i = 0; i < 16; i++)
            data[i] = Number((this.addr >> BigInt(8 * (15 - i))) & 0xffn);
        return data;
    }

    toString() {
        const data = this.toWire();

        const ht: string[] = [];
        const zero = IPv6Addr.findLongestZeroRun(data);

        for(let i = 0; i < 8; i++) {
            if (zero.i !== -1 && i >= zero.i && i < (zero.i + zero.len)) {
                if (i === zero.i) ht.push('');
                continue;
            }
            ht.push(data.readUInt16BE(i * 2).toString(16));
        }

        if (!ht[0].length) ht.unshift('');
        if (!ht[ht.length - 1].length) ht.push('');

        return `${ht.join(':')}${this.prefix !== 128 ? `/${this.prefix}` : ''}`;
    }
}


class Configuration {
    private config: Partial<Config>;
    id: string;
    key: string;
    proxy: any;
    address: IPv6Addr | null;
    private subnets: Generator | null;
    private nodeSubnet: IPv6Addr | null;

    constructor(config: Partial<Config>, id: BoxKeyPair) {
        this.config = config;

        this.id = Buffer.from(id.publicKey).toString('base64url');
        this.key = Buffer.from(id.secretKey).toString('base64');

        this.proxy = new Proxy(config, {
            get: (target, name, ...args) => {
                if (typeof (this as any)[name] === 'function') return (this as any)[name].apply(this);
                if (name in this) return (this as any)[name];
                return Reflect.get(target, name, ...args);
            }
        });

        this.address = this.config.wireguard ? IPv6Addr.parse(this.config.wireguard.Interface.Address) : null;

        // Create a generator for /64 subnets so that those subnets can be
        // allocated to various interfaces from templates.
        this.subnets = this.address ? this.address.subnets(64) : null;

        // The first subnet is reserved for the node itself
        this.nodeSubnet = this.subnets ? this.subnets.next().value : null;
    }

    seceSubnet() {
        return this.config.wireguard ? IPv6Addr.parse(this.config.wireguard.Peer.AllowedIPs) : null;
    }

    mappedIPv4() {
        return this.address ? this.address.subnet(96, 0x000000ffffn) : null;
    }

    nextSubnet() {
        if (!this.subnets) throw new Error('No subnets');
        const { value, done } = this.subnets.next();
        if (done) throw new Error('No more subnets');
        return value;
    }
}


export async function handler({ id, config, skip, templates }: { id: BoxKeyPair, skip: boolean, config: Partial<Config>, templates: string[] }) {
    const cfg = new Configuration(config, id);

    const res = await Promise.allSettled(templates.map(async dst => {
        const dir = path.dirname(dst);
        const name = path.basename(dst);
        const src = `${dir}/.${name}.sece`;
        return {
            dst, dir, name, src,
            text: sqrl.render(await fs.readFile(src, 'utf8'), cfg.proxy, {
                autoEscape    : false,
                autoTrim      : false,
                defaultFilter : 'neitherNullNorUndefined'
            })
        };
    }));

    let error = false;
    res.forEach((rv, i) => {
        let msg = `Processing template for ${templates[i]}: `;
        if (rv.status === 'fulfilled') msg += 'OK';
        else {
            error = true;
            msg += skip ? `skipping (${rv.reason})` : rv.reason;
        }
        console.log(msg);
    });
    if (!skip && error) throw new Error(`Template processing failed`);

    console.log(`Updating all configuration files`);
    await Promise.all(res.map(async v => {
        if (v.status !== 'fulfilled') {
            if (skip) return;
            throw new Error('Bug: Got a failed template result');
        }

        const { dst, dir, name, src, text } = v.value;

        // If there is a previous version of the destination file, back it up
        try {
            await fs.stat(dst);
            await fs.rename(dst, `${dir}/.${name}.old`);
        } catch(error) { /* nothing */ }

        // Copy uid, gid, and mode from the template file into the destination
        // file and write out the contents
        const stats = await fs.stat(src);
        await atomicWrite(dst, Buffer.from(text, 'utf8'), stats.mode, stats.uid, stats.gid);
    }));
}


async function loadTemplates(argv) {
    const { templates: dir } = argv;
    const paths = (await fs.readdir(dir)).map(n => path.join(dir, n));

    // Make sure that all paths are symbolic links
    await Promise.all(paths.map(async (l) => {
        const stat = await fs.lstat(l);
        if (!stat.isSymbolicLink())
            throw new Error(`Path '${l}' is not a symbolic link`);
    }));

    // Obtain the target (filename) of each symbolic link
    argv.templates = await Promise.all(paths.map(l => fs.readlink(l)));
}


export function builder(yargs) {
    yargs
        .option('templates', {
            alias: 't',
            default: '/etc/sece/templates',
            describe: 'Template directory',
            type: 'string'
        })
        .option('skip', {
            alias    : 's',
            describe : 'Skip templates that cannot be processed',
            default  : false,
            type     : 'boolean'
        })
        .middleware(ensureRoot)
        .middleware(loadIdentity)
        .middleware(loadTemplates);
}
