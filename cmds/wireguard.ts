import { throwForErrors } from '@janakj/lib/http';
import { fetchAuth } from '../auth.ts';
import { type Arguments } from 'yargs';
import { type IdArgs, ensureWritableConfig, loadIdentity, type WireGuardConfig } from '../config.ts';

export const command = 'wg';
export const aliases = 'wireguard';
export const describe = 'Manage WireGuard tunnel registration';


async function register({ config, api, id }: Arguments<IdArgs>) {
    const res = await fetchAuth(id, `${api}/wireguard`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            PublicKey : Buffer.from(id.publicKey).toString('base64')
        })
    });

    await throwForErrors(res);
    config.wireguard = await res.json() as WireGuardConfig;
}


async function unregister({ config, api, id }: Arguments<IdArgs>) {
    const pubkey = Buffer.from(id.publicKey).toString('base64');
    const res = await fetchAuth(id, `${api}/wireguard/${pubkey}`, { method: 'DELETE' });
    await throwForErrors(res);
    delete config.wireguard;
}


export function builder(yargs) {
    yargs
        .middleware(ensureWritableConfig)
        .middleware(loadIdentity)
        .command('register', 'Register WireGuard peer', (() => {}) as any, register as any)
        .command('unregister', 'Unregister WireGuard peer', (() => {}) as any, unregister as any)
        .demandCommand();
}
