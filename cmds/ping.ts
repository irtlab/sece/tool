import fetch from 'node-fetch';
import { throwForErrors } from '@janakj/lib/http';
import { randomInt } from '@janakj/lib/random';
import { type CmdlineArgs } from '../sece.ts';
import { type Arguments } from 'yargs';

export const command = 'ping';
export const describe = 'Test whether the SECE API endpoint is reachable';


function generateToken(length=16) {
    const t = Buffer.alloc(length);
    for(let i = 0; i < t.length; i++)
        t[i] = randomInt(0, 256);
    return t.toString('hex');
}


export async function handler({ api }: Arguments<CmdlineArgs>) {
    const txToken = generateToken();
    const res = await fetch(`${api}/ping?token=${txToken}`);
    await throwForErrors(res);

    const rxToken = (await res.text()).trim();
    if (rxToken != txToken) throw new Error('Invalid ping token');
    console.log('OK');
}
