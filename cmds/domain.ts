import fetch from 'node-fetch';
import { promisify } from 'util';
import childProcess, { type ExecOptions } from 'child_process';
import dns from 'dns';
import { promises as fs } from 'fs';
import { throwForErrors } from '@janakj/lib/http';
import sleep from '@janakj/lib/sleep';
import { loadIdentity, ensureWritableConfig, type AcmeDnsConfig, type IdArgs, type IdpConfig } from '../config.ts';
import { fetchAuth } from '../auth.ts';
import { ensureRoot } from '../sece.ts';
import { type Arguments } from 'yargs';
import { type BoxKeyPair } from 'tweetnacl';

export const command = 'dns';
export const aliases = 'domain';
export const describe = 'Manage DNS domain delegation';

const execFile = promisify(childProcess.execFile);
const resolve = promisify(dns.resolve);


interface DnsRec {
    name: string;
    type: 'CNAME' | 'A' | 'AAAA' | 'PTR';
    data: string[];
}


async function waitForEnter() {
    return new Promise((resolve) => {
        process.stdin.once('data', () => {
            process.stdin.resume();
            resolve(true);
            process.stdin.pause();
        });
    });
}


// No acme-dns credentials were found. Register a new acme-dns client and return
// the credentials so that they could be saved into the configuration file.
async function registerWithAcmeDns() {
    const rv = await fetch('https://auth.acme-dns.io/register', { method: 'POST' });
    await throwForErrors(rv);
    return (await rv.json()) as AcmeDnsConfig;
}


// Register a new OAuth2 client with the SECE OpenID Connect identity provider.
// The credentials (client_id, client_secret) will be saved in the configuration
// file. The client allows the node to register OAuth2 redirect_uris whenever
// the set of supported domains is changed.
async function createIdpClient(id: BoxKeyPair, api: string) {
    console.log('Registering new client with the SECE identity provider');
    const res = await fetchAuth(id, `${api}/idp/client`, { method: 'POST' });
    await throwForErrors(res);
    return await res.json() as IdpConfig;
}


// Update the list of redirect URIs registered at the SECE OpenID Connect
// provider to include all provided domains.
async function syncIdpRedirectURIs(id: BoxKeyPair, api: string, client_id: string, domains?: string[]) {
    const rv = await fetchAuth(id, `${api}/idp/client/${client_id}/redirect_uris`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify((domains || []).map(d => `https://vouch.${d}/auth`))
    });
    await throwForErrors(rv);
    return await rv.json() as string[];
}


async function ensureAcmeDns({ config }) {
    if (config.acmeDns === undefined) {
        console.log('No acme-dns account found, registering new one');
        config.acmeDns = await registerWithAcmeDns();
    }
}


async function getRequiredRecords(id: BoxKeyPair, api: string, domain: string) {
    const res = await fetchAuth(id, `${api}/domain/${domain}/required-records`);
    await throwForErrors(res);
    return await res.json() as any;
}


function dnsRecordToString(rec: DnsRec) {
    return rec.data.map(d => `${rec.name}  ${rec.type}  ${d}`).join('\n');
}


async function delegate({ config, api, domain, id, address }: Arguments<IdArgs & { domain: string, address?: string }>) {
    const rv = await getRequiredRecords(id, api, domain);
    if (!rv.isLocal) {
        // If the domain is not a locally-managed domain, we need to tell the
        // user to create the records "out of band".
        const rrs = [...rv.requiredRecords];
        if (config.acmeDns)
            rrs.push({
                name: `_acme-challenge.${domain}.`,
                type: 'CNAME',
                data: [`${config.acmeDns.fulldomain}.`]
            });
        console.log('Please configure the following DNS records:\n');
        console.log(rrs.map(dnsRecordToString).join('\n'));
        console.log('\nPress Enter to continue');
        await waitForEnter();

        // Now wait for the required records to propagate through DNS. If the
        // records do not exist or do not have the correct value, domain
        // delegation in the next step would fail.
        process.stdout.write('Waiting for DNS records to propagate...');
        await waitForDnsRecords(rv.requiredRecords);
        process.stdout.write(`all found.\n`);
    }

    console.log(`Delegating domain ${domain}`);
    const res = await fetchAuth(id, `${api}/domain`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            domain,
            ...(address ? { address } : null),
            ...(config.acmeDns ? { '_acme-challenge' : config.acmeDns.fulldomain } : null )
        })
    });
    await throwForErrors(res);
    console.log(`Domain delegated`);

    config.domains = config.domains || [];
    config.domains.push(domain);

    if (config.idp === undefined) {
        const { client_id, client_secret } = await createIdpClient(id, api);
        config.idp = { client_id, client_secret };
    }
    await syncIdpRedirectURIs(id, api, config.idp.client_id, config.domains);
}


async function release({ config, api, domain, id }: Arguments<IdArgs & { domain: string }>) {
    const res = await fetchAuth(id, `${api}/domain/${domain}`, { method: 'DELETE' });
    await throwForErrors(res);
    config.domains = (config.domains || []).filter(d => d !== domain);
    // Do not leave empty "domains" attribute behind
    if (!config.domains.length) delete config.domains;

    if (config.idp)
        await syncIdpRedirectURIs(id, api, config.idp.client_id, config.domains);
}


async function waitForDnsRecords(rrs: DnsRec | DnsRec[]) {
    if (!Array.isArray(rrs)) rrs = [rrs];
    for(;;) {
        try {
            const res = await Promise.allSettled(rrs.map(r => resolve(r.name, r.type)));
            const invalid = res.filter((r, i) => {
                if (r.status === 'rejected') return true;
                const a = r.value.map(v => `${v}.`).sort();
                const b = [...rrs[i].data].sort();
                return (JSON.stringify(a) !== JSON.stringify(b));
            });
            if (!invalid.length) return;
        } catch (error: any) {
            if (error.code !== 'ENOTFOUND') throw error;
        }
        await sleep(1000);
    }
}


function emitLines(stream) {
    let buffer = '';
    stream.on('data', data => {
        buffer += data;
        let n = buffer.indexOf('\n');
        while (n !== -1) {
            stream.emit('line', buffer.substring(0, n));
            buffer = buffer.substring(n + 1);
            n = buffer.indexOf('\n');
        }
    });
    stream.once('end', () => {
        if (buffer) stream.emit('line', buffer);
    });
}


function invoke(cmd: string, opts?: ExecOptions) {
    return new Promise((resolve, reject) => {
        const proc = childProcess.exec(cmd, opts);
        emitLines(proc.stdout);
        emitLines(proc.stderr);
        proc.stdout?.on('line', line => { console.log(`| ${line}`); });
        proc.stderr?.on('line', line => { console.error(`| ${line}`); });
        proc.once('close', code => {
            proc.stdin!.end();
            if (code === 0) {
                resolve(code);
            } else {
                const e = new Error(`Process returned ${code}`);
                (e as any).code = code;
                reject(e);
            }
        });
    });
}


async function installForNginx(domain: string) {
    // Nginx runs under www-data, so set the correct ownership, permission, and
    // reload command here. acme.sh preserves the ownership and permission of
    // the files it installs, so these must be set correctly here.

    await execFile('install', [
        '-o', 'www-data',
        '-g', 'www-data',
        '-m', '0444',
        '/dev/null', `/data/certs/${domain}.crt`
    ]);
    await execFile('install', [
        '-o', 'www-data',
        '-g', 'www-data',
        '-m', '0400',
        '/dev/null', `/data/certs/${domain}.key`
    ]);

    // We must specify the --reloadcmd option in the command below in order
    // for acme.sh to save the reload command in the configuration file for
    // later use in --cron during certificate renewal. This also means that
    // nginx must be running during 'sece domain certify`, otherwise the
    // reload command would fail.
    await invoke(`/etc/acme.sh/acme.sh --home /etc/acme.sh --install-cert -d '${domain}' --key-file '/data/certs/${domain}.key' --fullchain-file '/data/certs/${domain}.crt' --reloadcmd 'systemctl reload nginx'`, {
        env : { DOMAIN_PATH : `/data/acme.sh/${domain}` }
    });
}


async function install(domain: string) {
    console.log(`Installing certificate and key for ${domain}`);
    try {
        // Make sure that the target directory exists and is owned by root. Only
        // root (acme.sh) should be allowed to write, other users should be
        // allowed to read and enter the directory. We install all certificate
        // and key files into the directory and set their ownership and
        // permission according to the program that will access the certificate.
        // This is currently only nginx which runs under the user www-data.
        await execFile('install', [
            '-d',
            '-o', 'root',
            '-g', 'root',
            '-m', '0755',
            `/data/certs`
        ]);

        return installForNginx(domain);
    } catch(error) {
        throw new Error(`Failed to install certificate or key for ${domain}`);
    }
}


async function certify({ domain, config }: Arguments<IdArgs & { domain?: string }>) {
    let names: string[];
    if (domain) {
        names = [ domain ];
    } else {
        names = config.domains || [];
        if (!names.length) throw new Error(`No domain names found in SECE configuration file`);
    }

    for(const name of names) {
        const cname = `_acme-challenge.${name}`;
        process.stdout.write(`Waiting for DNS CNAME alias for ${cname}...`);
        await waitForDnsRecords({
            name: `_acme-challenge.${name}.`,
            type: 'CNAME',
            data: [`${config.acmeDns!.fulldomain}.`]
        });
        process.stdout.write('found.\n');
        console.log(`Obtaining certificate for ${name}...`);
        try {
            const { subdomain, username, password } = config.acmeDns!;
            await invoke(`umask 0077 && /etc/acme.sh/acme.sh --home /etc/acme.sh --dns dns_acmedns --issue -d '${name}' -d '*.${name}'`, {
                env : {
                    DOMAIN_PATH       : `/data/acme.sh/${name}`,
                    ACMEDNS_SUBDOMAIN : subdomain,
                    ACMEDNS_USERNAME  : username,
                    ACMEDNS_PASSWORD  : password
                }
            });
        } catch(error) {
            throw new Error(`Failed to obtain certificate for ${name}`);
        }
        console.log('Certification done.');
        await install(name);
    }
}


async function uncertify({ domain }: Arguments<IdArgs & { domain: string }>) {
    console.log(`Removing certificate for ${domain}`);
    try {
        await invoke(`/etc/acme.sh/acme.sh --home /etc/acme.sh --remove -d ${domain}`, {
            env: { DOMAIN_PATH : `/data/acme.sh/${domain}`}
        });
    } catch(error) {
        throw new Error(`Failed to remove certificate for ${domain}`);
    }
    const dir = `/data/acme.sh/${domain}`;
    console.log(`Removing ${dir}`);
    await fs.rm(dir, { recursive: true, force: true });
    console.log('done.');
}


export function builder(yargs) {
    yargs.command('delegate <domain> [address]', 'Delegate DNS domain to given key and IPv6 address', (yargs => {
        yargs
            .middleware(ensureWritableConfig)
            .middleware(ensureAcmeDns)
            .middleware(loadIdentity)
            .positional('domain', {
                describe : 'Domain name to delegate',
                type     : 'string'
            })
            .positional('address', {
                describe : 'IPv6 address',
                type     : 'string'
            });
    }) as any, delegate as any);

    yargs.command('release <domain>', 'Release DNS domain from the given key', (yargs => {
        yargs.middleware(ensureWritableConfig);
        yargs.middleware(loadIdentity);
        yargs.positional('domain', {
            describe : 'Domain name to release',
            type     : 'string'
        });
    }) as any, release as any);

    yargs.command('certify [domain]', 'Get certificate(s)', (yargs => {
        yargs.middleware(ensureRoot);
        yargs.middleware(ensureAcmeDns);
        yargs.middleware(ensureWritableConfig);
        yargs.positional('domain', {
            describe : 'Domain name to obtain certificate for',
            type     : 'string'
        });
    }) as any, certify as any);

    yargs.command('uncertify <domain>', 'Remove certificate(s)', (yargs => {
        yargs.middleware(ensureRoot);
        yargs.positional('domain', {
            describe : 'Domain name to remove certificate for',
            type     : 'string'
        });
    }) as any, uncertify as any);

    yargs.demandCommand();
}
