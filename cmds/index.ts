import * as configure from './configure.ts';
import * as domain from './domain.ts';
import * as ping from './ping.ts';
import * as wireguard from './wireguard.ts';

const commands = [
    configure, domain, ping, wireguard
];

export default commands;