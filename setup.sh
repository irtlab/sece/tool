#!/bin/bash
set -Eeuo pipefail

if [ "$(whoami)" != "root" ] ; then
    echo "This script must be run under root"
    exit 1
fi

error () {
    echo -en "\033[31mERROR: "
    echo -e "$@""\033[0m"
}

bail () {
    [ $# -ge 1 ] && error $@
    exit 1
}

text_input () {
    local text="$1"
    local init="${2-}"
    local len="$(echo -n "$1" | wc -m)"
    value=$(whiptail --inputbox --nocancel "$text" 8 $((len+4)) "$init" 3>&1 1>&2 2>&3)
}

dir="/data/wireguard"
id="$dir/id"
mkdir -p "$dir"

value="$(sece get accounts || /bin/true)"
text_input "Please enter comma-separated Google-hosted email accounts(s)" "$value"
[ -n "$value" ] || bail "Please provide at least one email account"
sece set accounts "$value"

value="$(sece get name || /bin/true)"
text_input "Please enter a human-friendly name for this machine" "$value"
[ -n "$value" ] || bail "Please provide a human-friendly name"
sece set name "$value"

dpkg-reconfigure tzdata
timezone="$(cat /etc/timezone 2>/dev/null)"
if [ -n "$timezone" ] ; then
    # Save the timezone string in the SECE configuration file so that it can be
    # re-created on OS update.
    sece set timezone "$timezone"
fi

# Create a WireGuard key if necessary
if [ ! -f "$id" ] ; then
    echo "Creating new WireGuard key"
    (umask 077; wg genkey > "$id"; chmod u-w "$id")
    wg pubkey < "$id" > "$id.pub"
    echo "Registering WireGuard key"
    sece wg register
else
    echo "Found existing WireGuard key, skipping registration"
fi

# Configure incremental backups
# Ask the user to select a Google Cloud region to backup to
value="$(sece get backup.region || /bin/true)"
text_input "Please select a backup region (us or eu)" "$value"
[ -n "$value" ] || bail "Please enter either us or eu"
sece set backup.region "$value"

# If there is no backup password in the configuration file, generate a new one.
backup_password="$(sece get backup.password || /bin/true)"
if [ -z "$backup_password" ] ; then
    sece set backup.password "$(pwgen -s 32)"
fi

# Delegate a domain if necessary
domains="$(sece get domains || /bin/true)"
if [ -z "$domains" ] ; then
    text_input "Please enter a domain to delegate"

    echo "Delegating domain $value to the node"
    sece domain delegate "$value"

    echo "Obtaining TLS server certificate for domain $value, this may take a while..."
    sece domain certify "$value"
else
    echo "Found existing domains, skipping domain delegation"
fi

echo "Re-generating configuration files"
sece configure -s

echo "Please reboot to apply all changes."
