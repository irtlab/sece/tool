import { promises as fs } from 'fs';
import nacl from 'tweetnacl';
import { testAtomicWrite, atomicWrite } from '@janakj/lib/fs';
import { type CmdlineArgs } from './sece.ts';


export interface AcmeDnsConfig {
    username: string;
    fulldomain: string;
    allowfrom: string[];
    password: string;
    subdomain: string;
}


export interface IdpConfig {
    client_id: string;
    client_secret: string;
    redirect_uris?: string[];
}


export interface WireGuardConfig {
    Peer: {
        PublicKey: string;
        AllowedIPs: string;
        Endpoint: string;
    },
    Interface: {
        Address: string;
    }
}


export interface Config {
    wireguard: WireGuardConfig;
    acmeDns: AcmeDnsConfig;
    domains: string[];
    idp: IdpConfig;
}


const config: {
    filename: string;
    contents: Partial<Config>;
    hash: string;
} = {} as any;


export type ConfigArgs = Omit<CmdlineArgs, 'config'> & { config: Partial<Config> };


function toJSON() {
    return JSON.stringify(config.contents, null, 4);
}


export async function loadConfig(argv) {
    const { config: filename } = argv;
    config.filename = filename;
    try {
        config.contents = JSON.parse(await fs.readFile(filename, 'utf8'));
    } catch(error: any) {
        if (error.code !== 'ENOENT') throw error;
        config.contents = {};
    }

    config.hash = Buffer.from(nacl.hash(new TextEncoder().encode(toJSON()))).toString('hex');
    argv.config = config.contents;
}


export function saveConfig() {
    if (config.filename === undefined)
        throw new Error('Bug: Invoke loadConfig middleware first');

    const text = toJSON();
    const hash = Buffer.from(nacl.hash(new TextEncoder().encode(text))).toString('hex');

    if (hash !== config.hash) {
        console.log(`Updating ${config.filename}`);
        return atomicWrite(config.filename, text);
    }
}


export function ensureWritableConfig() {
    if (config.filename === undefined)
        throw new Error('Bug: Invoke loadConfig middleware first');
    return testAtomicWrite(config.filename);
}


export type IdArgs = Omit<ConfigArgs, 'id'> & { id: nacl.BoxKeyPair };


export async function loadIdentity(argv) {
    const data = (await fs.readFile(argv.id)).toString().trim();
    argv.id = nacl.box.keyPair.fromSecretKey(Buffer.from(data, 'base64'));
}
